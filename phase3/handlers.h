//handlers.h, 159

#ifndef _HANDLERS_H_
#define _HANDLERS_H_

#include "data.h"
#include "types.h"  //need def of 'func_p_t' below

void NewProcHandler(func_p_t p);  //prototype of NewProcHandler
void TimerHandler(void);          //prototype for TimerHandler
void GetPidHandler(void);
void SleepHandler(void);
void WriteHandler(void);
void MutexLockHandler(void);
void MutexUnlockHandler(void);

#endif
