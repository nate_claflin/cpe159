//data.h, 159
//kernel data are all declared in main.c during bootstrap, but
//other kernel.c code must be referenced as 'extern'

#ifndef _DATA_H_                                    //name mangling prevention
#define _DATA_H_                                    //name mangling prevention

#include "types.h"                                  //defines q_t, pcb_t, PROC_NUM, PROC_STACK_SIZE

extern int run_pid;                                 //PID of current selected process, 0 is none
extern q_t ready_q, run_q;          //ready, runable, sleeping PID's, and list of semaphores
extern pcb_t pcb[PROC_NUM];                         //20 process control blocks
extern char proc_stack[PROC_NUM][PROC_STACK_SIZE];  //20 proc runtime stacks
extern unsigned int timer_tick;						//global counter value
extern mutex_t mutex;
extern int pies;
#endif
