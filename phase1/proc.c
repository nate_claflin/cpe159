//proc.c, 159
//all processes are coded here
//processes do not use kernel space (data.h) or code (handlers, tools)
//all must be done through system service calls

#include "spede.h"  //cons_xxx below needs
#include "data.h" //current_pid needed
#include "proc.h"   //prototypes of processes

void SystemProc(void) {
  int i;                        //counter var
  while(1) {                    //forever (until interrupt)
    cons_printf("0 ");          //so you know its in system proc
    for (i = 0; i < LOOP; i++) {
      asm("inb $0x80"); //about 1 sec delay
    }

  }
}
void UserProc(void) {
  int i;                        //counter var
  while(1) {                    //forever (until interrupt)
    cons_printf("%i ",run_pid);  //print the pid
    for (i = 0; i < LOOP; i++) {
      asm("inb $0x80");  //about 1 sec delay
    }
  }
}
