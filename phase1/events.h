//events.h of events.S

#ifndef _EVENTS_H_        //if events_H is not defined
#define _EVENTS_H_        //define it 

#define TIMER_EVENT 32    //define the interrupt vector for timer_event 

#ifndef ASSEMBLER         //if Assembler isnt defined
__BEGIN_DECLS
#include "types.h"        //proc_frame_t
                          //function prototypes for the trap events
void TimerEvent();        //coded in events.S, assembler won't like syntax
void ProcLoader(proc_frame_t *);  //coded in events.S
__END_DECLS

#endif                    //ifndef assemberl
#endif                    //ifndef _events_h_
