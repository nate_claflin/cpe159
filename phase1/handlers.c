//handlers.c, 159

#include "spede.h"
#include "types.h"
#include "data.h"
#include "tools.h"
#include "proc.h"
#include "handlers.h"

//to create process, alloc PID, PCB and process stack space
//build process frame, init PCB, record PID to run_q (if not 0)
void NewProcHandler(func_p_t p) { //arg: where proc code starts
  int pid;                                        //initialize pid selector
  if (ready_q.size == 0) {                        //if size of ready_q is 0, panic
    cons_printf("Kernel Panic: cannot create more processes\n");
    return;
  }
  pid = DeQ(&ready_q);                            //get a 'pid' from ready_q
  MyBzero((char *)&pcb[pid], sizeof(pcb_t));      //use tool function MyBzero to clear PCB and runtime stack
  MyBzero((char *)proc_stack, PROC_STACK_SIZE);   //clear out proc_stack data as well
  if (pid != 0) EnQ(pid, &run_q);                 //use EnQ to add the pid into run_q
  pcb[pid].proc_frame_p = (proc_frame_t *)&proc_stack[pid][PROC_STACK_SIZE - sizeof(proc_frame_t)];   //point proc_frame_p into stack
  pcb[pid].proc_frame_p->EFL = (unsigned int)(EF_DEFAULT_VALUE|EF_INTR);	//bit 9 is EF_INTR and bit 1 is EF_DEFAULT_VALUE
  pcb[pid].proc_frame_p->EIP = (unsigned int)p;// fill out EIP to p (can only be read through stack after call instruction)
  pcb[pid].proc_frame_p->CS = (unsigned int)get_cs();  //fill CS with the return from get_cs() call
}

void TimerHandler(void) { //count run_time of running process and preempt it if reaching time slice
  static unsigned int timer_tick = 0; //tick variable

  if (!(timer_tick % 75)) { //if timer is divisible by 75 (0.75 seconds)
    cons_putchar('.');      //put dot to show activity
  }
  
  timer_tick++;             //increment ticker
  outportb(0x20,0x60);      //dismiss timer event IRQ0;
  
  if (run_pid == 0) {       //if running process is SystemProc, return
    return;
  }
  
  pcb[run_pid].run_time++; //upcount cpu time of running process (think this syntax works)
  
  if (pcb[run_pid].run_time == TIME_SLICE) { //if it reaches time slice, upgrade/downgrade its state
    pcb[run_pid].state=RUN;  //(think this works, since running to ready is 1 to 0)
  
  
  EnQ(run_pid, &run_q);       //queue back to run_q
  run_pid = -1;             //reset the running pic to -1 (no processes running)
  }
}
