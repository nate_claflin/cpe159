//types.h, 159

#ifndef _TYPES_H_
#define _TYPES_H_
#define TIMER_EVENT 32  //IDT entry #32 has code addr for timer event

#define LOOP 16666660    //loop counter, duh
#define TIME_SLICE 200  //max timer count for proc
#define PROC_NUM 20     //maximum procs
#define Q_SIZE 20       //queueing capacity
#define PROC_STACK_SIZE 4096  //proc runtime stack in bytes

typedef void (*func_p_t)();  //void return function pointer type

typedef enum {READY, RUN} state_t; //define READY as 0 and RUN as 1

typedef struct {
  unsigned int PUSHA[8];        //holds all pushed registers
  unsigned int EIP;             //holds last instruction pointer
  unsigned int CS;              //holds code segment
  unsigned int EFL;             //holds cpu register flags
} proc_frame_t;                 //name type proc_frame_t

typedef struct {
  state_t state;                //state (READY, RUN)
  int run_time;                 //how much currently running
  int life_time;                //how long overall has run
  proc_frame_t *proc_frame_p;   //all frame data
  //pcb_t *parent;              //future use for forking
  //int sem_key;                //future use of a semaphore
  //int priority;               //future use of priority handling
} pcb_t;                        //name type pcb_t

typedef struct {
  int size;                     //size is where tail for new data goes
  int q[Q_SIZE];                //ints stored in q[] array
} q_t;                          //name type q_t

#endif
