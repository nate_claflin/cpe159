//data.h, 159
//kernel data are all declared in main.c during bootstrap, but
//other kernel.c code must be referenced as 'extern'

#ifndef _DATA_H_                                    //name mangling prevention
#define _DATA_H_                                    //name mangling prevention

#include "types.h"                                  //defines q_t, pcb_t, PROC_NUM, PROC_STACK_SIZE

extern int run_pid;                                 //PID of current selected process, 0 is none
extern q_t ready_q, run_q, sem_q;          //ready, runable, sleeping PID's, and list of semaphores
extern q_t terminal_buffer[2], terminal_wait_queue[2];
extern q_t term_kb_wait_q[2], term_screen_wait_q[2];
extern pcb_t pcb[PROC_NUM];                         //20 process control blocks
extern char proc_stack[PROC_NUM][PROC_STACK_SIZE];  //20 proc runtime stacks
extern unsigned int timer_tick;						//global counter value
extern mutex_t mutex;
extern int pies;
extern int term1_base;       //com2/3/4 IO bases: 0x2f8 0x3e8 0x2e8
extern int term2_base;
extern page_t page[PAGE_NUM];
extern int kernel_cr3;

#endif
