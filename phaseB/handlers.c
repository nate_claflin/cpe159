//handlers.c, 159

#include "spede.h"
#include "types.h"
#include "data.h"
#include "tools.h"
#include "proc.h"
#include "handlers.h"

void ExecHandler(proc_frame_t *proc_frame_p) {
  int i, handler;                         //temp variables
  char *main_table, *inst_subtable, *stack_subtable, *inst_page, *stack_page; //pointers to use array notation to set entries

  int pages[5];                           //total pages = 5
  int count = 0;                          //initialize counter;

  cons_printf("Made to execHandler\n");
  handler = (int)proc_frame_p->EBX;       //get the Aout address

                                          //find 5 unused memory pages
                                          /* 0 = instruction page
                                              1 = stack page
                                              2 = main table
                                              3 = instruction subtable
                                              4 = stack subtable
                                          */
  for (i = 0; i < PAGE_NUM; i++) {        //loop through pages
    if (page[i].owning_pid == -1) {       //if page has no owner, take it
      cons_printf("page[i].owning_pid = %i, i = %i, count = %i\n",page[i].owning_pid,i,count);
      pages[count] = i;                   //fill page array to use as an index
      page[i].owning_pid = run_pid;       //set owner to me
      if (count++ == 4) {                 //if count has been met, break
        cons_printf("breaking\n");
        break;
      }
    }
  }

  if ((i == PAGE_NUM) || (count != 5)) {  //if pages is over limit or count didnt succeed, exit
    cons_printf("Not enough available pages, Returning...\n");
    return;
  }

  main_table = (char *)page[pages[2]].addr; //main table
  inst_subtable = (char *)page[pages[3]].addr;  //instruction table
  stack_subtable = (char *)page[pages[4]].addr; //stack's subtable
  inst_page = (char *)page[pages[0]].addr;      //instruction's actual page
  stack_page = (char *)page[pages[1]].addr;     //stack's actual page

  MyMemcpy(inst_page, (char *)handler,  PAGE_SIZE);  //copy Aout to instruction page

  pcb[run_pid].proc_frame_p->EIP = 0x40000000;  //set instruction pointer to 1G
  MyMemcpy((char *)((int)stack_page + PAGE_SIZE - sizeof(proc_frame_t)),\
               (char *)pcb[run_pid].proc_frame_p, sizeof(proc_frame_t));    //copy proc frame to end stack page

  cons_printf("Main Table = %x\n",main_table);
  pcb[run_pid].proc_frame_p = (proc_frame_t *)((int)stack_page + PAGE_SIZE - sizeof(proc_frame_t)); //set proc frame address to 2G-1
  cons_printf("copy kernel to main table\n");
  MyMemcpy(main_table,(char *)(kernel_cr3), PAGE_SIZE); //copy kernel table into main table
  cons_printf("Inst Table = %x\n",inst_subtable);
  main_table[256] = (unsigned int)inst_subtable | 3;     //set 256 entry to address of inst subtable with 0b11 as mask for valid and r/w bits
  cons_printf("Main Table[256] = %x\n", main_table[256]);
  cons_printf("Stack Table = %x\n",stack_subtable);
  main_table[511] = stack_subtable | 3;    //set 511 entry to address of stack subtable with 0b11 as mask for valid and r/w bits
  cons_printf("Main Table[511] = %x\n", main_table[511]);
  cons_printf("Inst Page = %x\n",inst_page);
  inst_subtable[0] = inst_page | 3;        //set 0th entry to address of actual inst page with 0b11 as mask for valid and r/w bits
  cons_printf("Inst Table[0] = %x\n", inst_subtable[0]);
  cons_printf("Stack Page = %x\n",stack_page);
  stack_subtable[1023] = stack_page | 3;   //set last entry to address of actual stack page with 0b11 as mask for valid and r/w bits
  cons_printf("Stack Table[1023] = %x\n", stack_subtable[1023]);

  pcb[run_pid].cr3 = (int)main_table;           //set the cr3 address to main table location
  cons_printf("Main Table[0] = %x\n", main_table[0]);
  cons_printf("Main Table[1] = %x\n", main_table[1]);
  cons_printf("Main Table[2] = %x\n", main_table[2]);
  cons_printf("Main Table[3] = %x\n", main_table[3]);
  set_cr3(pcb[run_pid].cr3);               //set the address translation table to main_table
  cons_printf("Exiting exechandler\n");
}

void ExitHandler(proc_frame_t *proc_frame_p) {
  int ppid, child_exit_num, *parent_exit_num_p;

  ppid = pcb[run_pid].ppid;           //set parent ID from child pcb

  if (pcb[ppid].state != WAITCHLD) {  //if parent isn't in WAITCHLD state
    pcb[run_pid].state = ZOMBIE;      //then child is a zombie
    
    run_pid = ppid;                   //reset run_pid to ? i think -1?

    if (pcb[ppid].sigchld_handler) {  //if parent has a child handler
      InsertWrapper(ppid, pcb[ppid].sigchld_handler); //insert wrapper with that handler (CallWaitPidNow())
    }
  }
  else {                            //otherwise, parent is waiting for child to exit
    pcb[ppid].state = RUN;          //release process
    EnQ(ppid, &run_q);              //put back to run queue

    pcb[ppid].proc_frame_p->ECX = run_pid; //1. deliver exiting child pid to parent
    
    child_exit_num = pcb[run_pid].proc_frame_p->EBX; //exit num = pid * 100 called from proc.c
    parent_exit_num_p = (int *)pcb[ppid].proc_frame_p->EBX; //set pointer address of int in function
    *parent_exit_num_p = child_exit_num;    //2. deliver to parent process the exiting child PID
    
    EnQ(run_pid, &ready_q);     //kernel reclaims child process PID
    pcb[run_pid].state = READY; //kernel reclaimes child process state
    run_pid = -1;               //reset PID to -1
    set_cr3(kernel_cr3);        //return back to regular kernel address table after resetting pid
  }
}

void WaitPidHandler(proc_frame_t *proc_frame_p) {
  int child_exit_num, *parent_exit_num_p;
  int child_pid = 0;                //initialize child_pid for test case
  int i;                            //loop variable

  for (i = 0; i < PROC_NUM; i++) {  //for list of pcb's
    if ((pcb[i].state == ZOMBIE) && (pcb[i].ppid == run_pid)) { //if there is a zombie process and its parent is running
      child_pid = i;                //found child pid, set it
      break;                        //leave loop
    }
  }
  if (!child_pid) {                 //if child pid was 0
    pcb[run_pid].state = WAITCHLD;  //need to wait for child to exit, set state
    run_pid = -1;                   //reset pid
  } else {                          //zombie child found
    proc_frame_p->ECX = (unsigned int)child_pid;        //deliver to parent process: 2. ZOMBIE PID
    set_cr3(pcb[child_pid].cr3);    //switch to child's address translation table to get exit code
    child_exit_num = (int)pcb[child_pid].proc_frame_p->EBX;     //exit code from proc.c call
    set_cr3(kernel_cr3);            //switch back to kernel address translation table
    parent_exit_num_p = (int *)proc_frame_p->EBX; //deliver to parent process: 1. ZOMBIE's exiting number
    *parent_exit_num_p = child_exit_num;  //set exit num to value parent pointing to
    
    EnQ(child_pid,&ready_q);      //1. its PID (enqueue it to ?)
    pcb[child_pid].state = READY; //2. its state (changed back to ?)
    run_pid = -1;                  //reset pid
  }
}

void InsertWrapper(int pid, func_p_t handler) {

  int *p;
  proc_frame_t temp_frame;
  temp_frame = *pcb[pid].proc_frame_p;    //a. copy the process frame to a local temp frame
  p = &pcb[pid].proc_frame_p->EFL;        //set pointer to the last value in proc frame
  *p = (int)handler;                      //b. copy 'handler' to where the last value of the old process frame            
  *(--p) = temp_frame.EIP;                //c. copy the EIP in temp frame to below 'handler'
  pcb[pid].proc_frame_p = (proc_frame_t *)((int)pcb[pid].proc_frame_p - sizeof(int [2])); //d. lower the process frame address by 2 integers
  MyMemcpy((char *)pcb[pid].proc_frame_p, (char *)&temp_frame, sizeof(proc_frame_t)); //e. copy temp frame back to where new process frame is
  pcb[pid].proc_frame_p->EIP = (unsigned int)Wrapper; //f. change EIP in process frame to Wrapper (programmed in proc.c)
}

void SignalHandler(proc_frame_t *proc_frame_p) {  //handler for signal, takes process temp_frame
  if (proc_frame_p->EBX == SIGINT) {              //if signal is ctrl-c
    pcb[run_pid].sigint_handler = (func_p_t)proc_frame_p->ECX;  //put the syscall value into the pcb 
  } 
  else if (proc_frame_p->EBX == SIGCHLD) {
    pcb[run_pid].sigchld_handler = (func_p_t)proc_frame_p->ECX; //put child handler in pcb
  }
}

void ForkHandler(proc_frame_t *proc_frame_p) {
  int child_pid, delta, *bp;      //working variables
  proc_frame_t *child_frame_p;    //new proc frame for child
  pcb[run_pid].proc_frame_p->EBX = 0;
  if (ready_q.size == 0) {        //if there is no more available processes
    cons_printf("Kernel Panic: Cannot create more processes!\n"); //print error
    pcb[run_pid].proc_frame_p->EBX = -1;      //return -1 to fork call
    return;                       //return
  }

  child_pid = DeQ(&ready_q);      //otherwise, get child id from ready queue                 
  EnQ(child_pid, &run_q);         //put into run queue                 
  MyBzero((char *)&pcb[child_pid], sizeof(pcb_t));  //clear out the pcb     
  pcb[child_pid].state = RUN;     //set state to run
  MyMemcpy(proc_stack[child_pid], proc_stack[run_pid], PROC_STACK_SIZE);  //copy entire runtime stack contents
  delta = proc_stack[child_pid] - proc_stack[run_pid];  //find distance between parent and child 
  child_frame_p = (proc_frame_t *)((int)pcb[run_pid].proc_frame_p + delta);   //set the location of child frame to parent plus delta
  pcb[child_pid].proc_frame_p = child_frame_p;    //set the child frame into pcb
  pcb[child_pid].ppid = run_pid;                  //set the child's parent ID into pcb
  pcb[child_pid].proc_frame_p->ESP = pcb[child_pid].proc_frame_p->ESP + delta;  //set stack pointer + distance
  pcb[child_pid].proc_frame_p->EBP = pcb[child_pid].proc_frame_p->EBP + delta;  //etc
  pcb[child_pid].proc_frame_p->ESI = pcb[child_pid].proc_frame_p->ESI + delta;  //etc
  pcb[child_pid].proc_frame_p->EDI = pcb[child_pid].proc_frame_p->EDI + delta;  //etc
  pcb[child_pid].sigint_handler = pcb[run_pid].sigint_handler;
  pcb[child_pid].cr3 = pcb[run_pid].cr3;

  bp = (int *)pcb[child_pid].proc_frame_p->EBP; //set the bp pointer to the contents of child's ebp
  while (*bp) {  //while the data bp points to is not 0        
    *bp += delta; //increment the contents of the pointer
    bp = (int *)*bp;  //make new reference to next level in BP chain                    
  }
  pcb[run_pid].proc_frame_p->EBX = child_pid;   //fill register to return to fork syscall
}

void PutCharHandler(int fileno) {
  int i, term;
  char ch;
  if (fileno == TERM1) {
    i = 0;
    term = TERM1_BASE;
  } else {
    i = 1;
    term = TERM2_BASE;
  }
  ch = pcb[run_pid].proc_frame_p->ECX;
  outportb(term, ch);
  EnQ(run_pid, &(term_screen_wait_q[i]));
  pcb[run_pid].state = WAIT;
  run_pid = -1;
}

void GetCharHandler(int fileno) {
 //check for char buffer
 int i;                                     //variable to set which term to pring
 char c;
 if (fileno == TERM1) i = 0;                //if term1, set i to 0
 else i = 1;                                //otherwise i to 1

 if (terminal_buffer[i].size == 0) {        //if there's nothing in the buffer, then need to wait
    EnQ(run_pid,&(term_kb_wait_q[i]));  //put the requesting PID in terminal wait queue
    pcb[run_pid].state = WAIT;           //block the requesting PID
    run_pid = -1;                           //reset PID and continue
 }
 else {                                     //otherwise there is something in the buffer queue
  c = DeQ(&terminal_buffer[i]); //pop value off into the ECX register
   pcb[run_pid].proc_frame_p->ECX = c;
 }
}

void TermHandler(int port) {
  int i,pid,indicator;                                //temp variables for storage
  char ch;                                  //temp char for returning
  if (port == TERM1_BASE) i = 0;            //if term1 base given, i = 0
  else i = 1;                               //otherwise i = 1
  indicator = inportb(port + IIR);

  if (indicator == IIR_RXRDY) {
    ch = inportb(port + DATA);                //get the character data from serial port
    if (term_kb_wait_q[i].size == 0) {   //if no terminals waiting for letter
      EnQ(ch,&terminal_buffer[i]);            //put letter in buffer
    } else {                                  //otherwise
      pid = DeQ(&term_kb_wait_q[i]);     //pop off the PID waiting from terminal
      EnQ(pid, &run_q);                       //put pid in run queue
      pcb[pid].state = RUN;                   //set state from WAIT to RUN 
      pcb[pid].proc_frame_p->ECX = ch;        //give character to the register
      if ((ch == (char)3) && (pcb[pid].sigint_handler)) {
        InsertWrapper(pid, pcb[pid].sigint_handler);
      } else {
        outportb(port + DATA, ch);                //write that character to screen
      }
    }
  } 

  else {
    if (term_screen_wait_q[i].size > 0) {   //if terminal is waiting for letter
      pid = DeQ(&term_screen_wait_q[i]);     //pop off the PID waiting from terminal
      EnQ(pid, &run_q);                       //put pid in run queue
      pcb[pid].state = RUN;                   //set state from WAIT to RUN 
    }
  }

  if (i) outportb(0x20,0x64); 
  else outportb(0x20,0x63);
}

void GetPidHandler(void) { 

  pcb[run_pid].proc_frame_p->EAX = run_pid; //fill register value of process frame for run_pid
}

void WriteHandler(void) {
  int i,j,fileno;
  char *p;
  j = 0;

  fileno = pcb[run_pid].proc_frame_p->EBX;        	//get fileno from syscall
  p = (char *)pcb[run_pid].proc_frame_p->ECX;       //get the message address from syscall
  if (fileno == STDOUT) {                         	//if buffer is STDOUT
    cons_printf("%s\n", p);                       	//cons_printf the string
  } else if (fileno == TERM1) {                   	//if fileno is terminal 1
    while (p[j] != '\0') {                          //while there's still string
      outportb(TERM1_BASE, p[j]);                  	//send char to term1 base address, increment pointer
      j++;
      for (i = 0; i < 3000; i++) asm("inb $0x80"); 	//delay for 1 sec
  	}
  } else if (fileno == TERM2) {                   	//if fileno is terminal 2
    while (p[j] != '\0') {                          //while there's still string
      outportb(TERM2_BASE, p[j]);                   //send char to term1 base address, increment pointer
      j++;
      for (i = 0; i < 3000; i++) asm("inb $0x80");  //delay for 1 sec
    }
    
  }
}

void SleepHandler(void) {
  int time;
  time = timer_tick + (pcb[run_pid].proc_frame_p->EBX * 100); //get the time from register 
  pcb[run_pid].wake_time = time;      			  //and add to current system time
  pcb[run_pid].state = SLEEPING;                  //set state
  run_pid = -1;                                    //reset run_pid
}

void NewProcHandler(func_p_t p) { //arg: where proc code starts
  int pid;                                        //initialize pid selector
  if (ready_q.size == 0) {                        //if size of ready_q is 0, panic
    cons_printf("Kernel Panic: cannot create more processes\n");
    return;
  }
  pid = DeQ(&ready_q);                            //get a 'pid' from ready_q
  MyBzero((char *)&pcb[pid], sizeof(pcb_t));      //use tool function MyBzero to clear PCB and runtime stack
  MyBzero((char *)&proc_stack[pid], PROC_STACK_SIZE);   //clear out proc_stack data as well
  if (pid != 0) EnQ(pid, &run_q);                 //use EnQ to add the pid into run_q
  pcb[pid].proc_frame_p = (proc_frame_t *)&proc_stack[pid][PROC_STACK_SIZE - sizeof(proc_frame_t)];   //point proc_frame_p into stack
  pcb[pid].proc_frame_p->EFL = (unsigned int)(EF_DEFAULT_VALUE|EF_INTR);	//bit 9 is EF_INTR and bit 1 is EF_DEFAULT_VALUE
  pcb[pid].proc_frame_p->EIP = (unsigned int)p;// fill out EIP to p (can only be read through stack after call instruction)
  pcb[pid].proc_frame_p->CS = (unsigned int)get_cs();  //fill CS with the return from get_cs() call
  pcb[pid].state = RUN;
  pcb[pid].cr3 = kernel_cr3;
}

void MutexLockHandler(void) {
  if (mutex.flag) {                               //if the mutex is free (1) 
    mutex.flag = 0;                               //set mutex as locked
  } 
  else {                                          //otherwise, mutex already locked
    EnQ(run_pid,&(mutex.wait_q));                 //put the requesting PID in wait queue
    pcb[run_pid].state = WAIT;                //set sleeping state, maybe should be WAIT so timerhandler doesnt try to recover
    run_pid = -1;                                 //reset so scheduler can schedule another process
  }
}

void MutexUnlockHandler(void) {
  int pid;
  if (mutex.wait_q.size == 0) {                   //if the wait queue has nothing in it 
    mutex.flag = 1;                               //reset the mutex flag, unlock it. 
  }
  else {                                          //if there are waiting processes
    pid = DeQ(&(mutex.wait_q));                   //get the waiting process
    pcb[pid].state = RUN;                         //set the state to running 
    EnQ(pid,&run_q);                              //enq into run queue for kernel to handle
  }
}

void TimerHandler(void) {                         //count run_time of running process and preempt it if reaching time slice
  int j;
  timer_tick++;
  for (j = 1; j < 20; j++) {                      //loop through pcb to find sleeping tasks
    if ((pcb[j].state == SLEEPING) && (timer_tick >= pcb[j].wake_time)) {  //if state is sleeping and time is up
        EnQ(j, &run_q);                           //queue back to run queue
        pcb[j].state = RUN;                     //alter state from SLEEPING (need to be RUN?)
    }
  }
  outportb(0x20,0x60);                            //dismiss timer event IRQ0;
  
  if (run_pid == 0) {                             //if running process is SystemProc, return
    return;
  }
  
  pcb[run_pid].run_time++;                        //upcount cpu time of running process (think this syntax works)
  
  if (pcb[run_pid].run_time == TIME_SLICE) {      //if it reaches time slice, upgrade/downgrade its state
    EnQ(run_pid, &run_q);                         //queue back to run_q
    run_pid = -1;                                //reset the running pic to -1 (no processes running)
  }
}
