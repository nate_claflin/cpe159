//proc.h, 159

#ifndef _PROC_H_
#define _PROC_H_
void SystemProc(void);  //prototypes
void ShellProc(void);
void Wrapper(func_p_t sig_handler);
void Ouch(void);
void CallWaitPidNow(void);

#endif
