// main.c, 159
// OS bootstrap and kernel code for OS phase 1
//
// Team Name: RNEA (Members: Nathaniel Claflin, Ryan Mansfield)

#include "spede.h"  //spede stuff
#include "types.h"  //data types
#include "events.h" //kernel events
#include "tools.h"  //small handler functions
#include "proc.h" //process names like SystemProc()
#include "handlers.h" //handler code

//kernel data are all declared here
int run_pid;
q_t ready_q, run_q,sleep_q;
pcb_t pcb[PROC_NUM];
char proc_stack[PROC_NUM][PROC_STACK_SIZE];
unsigned int timer_tick;

void ProcScheduler(void) {
  if ((run_pid > 0) && (pcb[run_pid].state != SLEEPING)) return;       //if run_pid is > 0, return (not a user proc, no scheduling needed)
  if (run_q.size == 0) run_pid = 0;  //if run_q is empty let run_pid be zero
  else run_pid = DeQ(&run_q);       //otherwise get first entry in run_q
  pcb[run_pid].life_time = pcb[run_pid].life_time + pcb[run_pid].run_time;  //accumulate life time by adding run_time and reset run_time to 0
  pcb[run_pid].run_time = 0 ;   //set run_time to zero
}

int main(void) {                //OS bootstrap
  int i;                        //counter variable
  struct i386_gate *IDT_p;      //dram where idt is

  timer_tick = 0;          //initialized during bootstrap
  run_pid = -1;                 //needs to find runnable pid
  MyBzero((char*)&run_q,sizeof(q_t));     //clear the ready_queue
  MyBzero((char*)&ready_q,sizeof(q_t));   //clear the run_queue
  for(i=0;i<Q_SIZE;i++){        //enqueue 0-19 to ready_q (all PIDs ready)
    EnQ(i,&ready_q);            //uses the EnQ helper, fills with empty proc PIDs 
  }

  IDT_p = get_idt_base();       //get the IDT_p (to point to/locate idt like in lab ex)
  cons_printf("IDT Located at DRAM addr %x (%d).\n",(int)IDT_p,(int)IDT_p);   //show location on Target PC  
  //fill IDT with custom event, ie 132
  
  fill_gate(&IDT_p[SYSCALL_EVENT], (int)SyscallEvent, get_cs(), ACC_INTR_GATE,0); //On syscall event

  fill_gate(&IDT_p[TIMER_EVENT], (int)TimerEvent, get_cs(), ACC_INTR_GATE,0); //On timerevent
  outportb(0x21,~1);            //set PIC mask to open up timer event signal irq0
  NewProcHandler(SystemProc);   //call NewProcHandler(SystemProc) to create 1st process, systemProc is func_ptr_p
  ProcScheduler();              //call ProcScheduler() to select from run_queue
  ProcLoader(pcb[run_pid].proc_frame_p); //call ProcLoader with proc_frame_p of selected run_pid
  return 0;
}

void Kernel(proc_frame_t *proc_frame_p) {  //called by TimerEvent in events.S 
   char key;                              //key pressed variable
   pcb[run_pid].proc_frame_p = proc_frame_p;  //save the current PCB frame before Timer called

   //handle interrupts if they are present. 
   if(pcb[run_pid].proc_frame_p->EVENT_TYPE== TIMER_EVENT) //if the event type is timer event
   {
     TimerHandler();                        //call timerhandler
   }
   else
   {
      switch(pcb[run_pid].proc_frame_p->EAX) {
          case GETPID:  
            GetPidHandler();
            break;
          case SLEEP:
            SleepHandler();
            break;
          case 4:
            WriteHandler();
            break;
          default:
            break;
       }
   }

   //load a new user process if the keyboard is hit
   //kernel gets called every timer event
   //
   
   if(cons_kbhit()) {                     //if Target PC keyboard pressed
     key = cons_getchar();                //convert key pressed to char for checking
     switch(key) {                        //begin CASE statements
       case 'n':                          //if 'n' key pressed
         NewProcHandler(UserProc);        //make a new USER PROC
         break;                           //break out of CASE
       case 'b':                          //if b key pressed
         breakpoint();                    //use built in GDB break function
         break;                           //break out of function (dont think necessary but not sure)
       default:
         break;
     }
   }
   
   ProcScheduler();                       //check whether a new PID needs to be called or not
   ProcLoader(pcb[run_pid].proc_frame_p); //load either previous or new run_pid into events.S 
}
