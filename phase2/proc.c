//proc.c, 159
//all processes are coded here
//processes do not use kernel space (data.h) or code (handlers, tools)
//all must be done through system service calls

#include "syscalls.h" //syscalls library

void SystemProc(void) {
  while(1) asm("inb $0x80");    //forever (until interrupt)
}

void UserProc(void) {
  char my_str[] = "  ";         //2 spaces for readability
  int temp; 

  while(1) {
    //forever (until interrupt)
    temp = GetPid();
    if(temp >= 10){
      my_str[0] = temp/10 + '0';
      my_str[1] = temp%10 + '0'; 
    }
    else{
      my_str[0] = temp + '0'; //fill out 1st space w/ service call and conver to ASCII
    }
    Write(STDOUT, my_str);      //print to screen w/ service call
    Sleep(((GetPid() % 5)+1));       //make process sleep for mod 5 seconds (max 5)
  }
}
