#define LOOP 1664435
#include "spede.h"
#include "events.h"

typedef void (* funct_ptr_t)(); //declare void type as empty function pointer
struct i386_gate *IDT_p; //Interrupt table struct 

void RunningProcess(void) {
  int i; //counter variable

  while(1) {
    if(cons_kbhit()) {  //if handlers.c sends character, break forever loop
      break;
    } else {
      for(i = 0; i<LOOP; i++) asm("inb $0x80"); //read in d'128 to simulate delay
      cons_putchar('z'); //put z's which fills screen
    }
  }
}
int main() {
  int i; //counter variable
  IDT_p = get_idt_base(); //get the interrupt table address
  cons_printf("IDT located at DRAM addr %x (%d).\n",IDT_p, (int)IDT_p);
  for(i = 0; i < 20; i++) {
    cons_printf("\n"); //put the debug message at top of screen
  }
  fill_gate(&IDT_p[TIMER_EVENT], (int)TimerEvent, get_cs(), ACC_INTR_GATE, 0);
  outportb(0x21, ~1); //PIC mask
  asm("sti"); //enable interrupts

  while(1) {
    RunningProcess(); //forever loop to wait for input
  }
  return 0;
}
