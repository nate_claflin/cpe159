#include "spede.h"

char my_name[] = "Nate Claflin"; //name
int i = 0; //first counter var
int j = 0; //second counter var
int tick_count = 0; //wait counter var

//points to video memory location, accessed directly
unsigned short *char_p = (unsigned short *) 0xB8000+12*80+34;

//subroutine to print name letter by letter
void TimerHandler() {
  //if tick count is new/reset
  if (tick_count == 0) {
    //place the value (with mask) directly into video memory
    //don't need printf here
    char_p[i] = my_name[i]+0xf00; 
  }
  //increment tick count to begin waiting
  tick_count++;
  //if .75 seconds have elapsed
  if (tick_count == 75) {
    //reset tick count
    tick_count = 0;
    //increment name index
    i++;
    //if the full name has been printed
    if (i == strlen(my_name)) {
      //reset counter
      i = 0;
      //erase all video memory for the length of name
      for (j = strlen(my_name); j >= 0; j--) {
        char_p[j]  = ' ';
      }
    }
  }
  //clears interrupt IRQ 0 so it can recieve more interrupts
  outportb(0x20, 0x60);
}
