//event.S initial stage handling an event

#include <spede/machine/asmacros.h> //do not use spede.h
#include "events.h"     //for K_DATA, K_STACK_SIZE below

.comm k_stack, 8192, 1  //declare kernel stack space (8192 x 1 byte)
.text

ENTRY(ProcLoader)
  movl 4(%esp),%eax   
  movl %eax, %esp     //copy eax to stack pointer, tells sp what interrupt to call
  popa                //pop all registers into frame
  add $4,%esp         //need to move past event_type in proc frame
  iret                //call interrupt return which pops eip, cs and eflags

ENTRY(TimerEvent)     //TimerEvent called/ HARDWARE REGS PUSHED HERE
  pushl $32            //push value of 32 for timer event
  jmp TheRest         //jump to therest entry

ENTRY(SyscallEvent)
    pushl $128         //push value of 128 for syscall event
    jmp TheRest       //jump to therest entry

ENTRY(Term1Event)
  pushl $35
  jmp TheRest

ENTRY(Term2Event)
  pushl $36
  jmp TheRest

ENTRY(TheRest)
  pusha               //push eflag, cs, eip, hardware
  movl %esp, %ebx     //move stack ptr to BX
  cld                 //clear direction flag
  
  movw $0x10, %ax     //set kernel data segment registers
  mov %ax, %ds        //set kernel data segment registers
  mov %ax, %es        //set kernel data segment registers
  leal k_stack + 8192, %esp //point ESP to kernel stack

  pushl %ebx          //push ebx to kernel stack
  call CNAME(Kernel)  //call kernel code
