//types.h, 159

#ifndef _TYPES_H_
#define _TYPES_H_
#define TIMER_EVENT 32  //IDT entry #32 has code addr for timer event
#define TERM1_EVENT 35
#define TERM2_EVENT 36
#define SYSCALL_EVENT 128

#define WRITE 4         //opcode for write operation
#define GETPID 100      //opcode for GetPid operation
#define SLEEP 101       //opcode for Sleep operation
#define STDOUT 1        //opcode for write location

#define LOOP 1666666    //loop counter, duh
#define TIME_SLICE 200  //max timer count for proc
#define PROC_NUM 20     //maximum procs
#define Q_SIZE 20       //queueing capacity
#define PROC_STACK_SIZE 4096  //proc runtime stack in bytes

#define MUTEX 102
#define LOCK 0
#define UNLOCK 1

#define TERM1 3
#define TERM2 4
#define TERM1_BASE 0x2f8
#define TERM2_BASE 0x3e8

#define GETCHAR 103
#define PUTCHAR 104

typedef void (*func_p_t)();  //void return function pointer type

typedef enum {READY, RUN, SLEEPING, WAIT} state_t; //define READY as 0 and RUN as 1

typedef struct {
  unsigned int FOUR[4];        //holds pushed registers
  unsigned int EBX;            //holds information for service call
  unsigned int EDX;            //holds information for service call
  unsigned int ECX;            //holds information for service call
  unsigned int EAX;            //holds interrupt data for service call
  unsigned int EVENT_TYPE;      //holds interrupt type pushed by EVENT routine
  unsigned int EIP;             //holds last instruction pointer
  unsigned int CS;              //holds code segment
  unsigned int EFL;             //holds cpu register flags
} proc_frame_t;                 //name type proc_frame_t

typedef struct {
  state_t state;                //state (READY, RUN)
  int run_time;                 //how much currently running
  int life_time;                //how long overall has run
  int wake_time;                //how long to wait
  proc_frame_t *proc_frame_p;   //all frame data
} pcb_t;                        //name type pcb_t

typedef struct {
  int size;                     //size is where tail for new data goes
  int q[Q_SIZE];                //ints stored in q[] array
} q_t;                          //name type q_t

typedef struct {
  int flag;                     //for process control
  q_t wait_q;                   //queue for blocked PIDs
} mutex_t;
#endif
