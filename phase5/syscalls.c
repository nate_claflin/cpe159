// syscalls.c
// API calls to kernel system services
#include "spede.h"
#include "types.h"
#include "data.h"
#include "tools.h"
#include "proc.h"
#include "handlers.h"

char GetChar(int fileno) {
  int key;
  asm("pushl %%eax;
      pushl %%ebx;
      pushl %%ecx;
      movl $103, %%eax;
      movl %1, %%ebx;
      int $128;
      movl %%ecx, %0;
      popl %%ecx;
      popl %%ebx;
      popl %%eax"
      : "=g" (key)
      : "g" (fileno)
      
      );
   return (char) key;
}
void Mutex(int service) {
  asm("pushl %%eax;
    pushl %%ebx;
    movl $102, %%eax;
    movl %0, %%ebx;
    int $128;
    popl %%ebx;
    popl %%eax"
    :  
    : "g" (service)
    );
}
/* GetPid takes no arguments, and pushes
  eax, moves 100 as opcode, then calls int
  service which will fetch the current pid
  and return into the eax register, then pops
*/

int GetPid(void) {
   int pid;

   asm("pushl %%EAX;        
        movl $100, %%EAX;   
        int $128;           
        movl %%EAX, %0;     
        popl %%EAX"         
       : "=g" (pid)         
       :                   
    );
   //int 128 is what calls the interrupt located at 128

   return pid;
}

/*Write pushes eax, then moves 4 for opcode
  then moves first argument into ebx, and second
  into ecx, then calls interrupt service, then pops
  eax back once completed
*/
void Write(int fileno, char *p) {
  asm("pushl %%eax;
      pushl %%ebx;
      pushl %%ecx;
      movl $4, %%eax;
      movl %0, %%ebx;
      movl %1, %%ecx;
      int $128;
      popl %%ecx;
      popl %%ebx;
      popl %%eax"
       :          // no outputs, otherwise, use "=g" (...)
       : "g" (fileno), "g" ((int)p)  
       );
 
}

/*sleep pushes eax, then moves 101 for opcode
  then moves first argument into ebx, and second
  into ecx, then calls interrupt service, then pops
  eax back once completed
*/
void Sleep(int time) {
  asm("pushl %%eax;
      pushl %%ebx;
      movl $101, %%eax;
      movl %0, %%ebx;
      int $128;
      popl %%ebx;
      popl %%eax"
      :
      : "g" ((int)time)
      );
}
