//proc.c, 159
//all processes are coded here
//processes do not use kernel space (data.h) or code (handlers, tools)
//all must be done through system service calls
#include "data.h"
#include "syscalls.h" //syscalls library

char* ascii_fixer(char *my_str, int temp) {
   if(temp >= 10){
     my_str[0] = temp/10 + '0';
     my_str[1] = temp%10 + '0';
  } else {
    my_str[0] = temp + '0';
    my_str[1] = ' ';
  }
  return my_str;
}
void SystemProc(void) {
  while(1) asm("inb $0x80");    //forever (until interrupt)
}

void UserProc(void) {
  char temp_str[] = "  ";
  char key;
  while(1) {                         //forever (until interrupt)
    char my_str[] = "  : Do you love cpe159?\r\n";              //2 spaces for readability
    ascii_fixer(temp_str,GetPid());    //get cleaned and fixed id number
    my_str[0] = temp_str[0];
    if (temp_str[1] != '\0') my_str[1] = temp_str[1];
    else my_str[1] = ' ';
    if (GetPid() % 2) {              //if pid is odd
      Write(TERM1, my_str);          //print to terminal 1 with my_str
      key = GetChar(TERM1);
      Write(STDOUT,(char *)&key);
      
    } else {       //if pid is even
      Write(TERM2, my_str);   //print to terminal 1 with string (need to figure out exactly what, no demo.dli)
      key = GetChar(TERM2);
      Write(STDOUT,&key);

    }
  }
}

void CookerProc(void) {
  char cook_too_many[] = "++++++++ Cooker  : too many pies!"; //error message with blank space for number
  char cook_make_pie[] = "Cooker  : making pie #  ....";      //notification message w/ blank space for numbers
  char my_str1[] = "  ";              //empty array for GetPid()
  char my_str2[] = "  ";              //empty array for ++pies

  while (1) {                         //forever (until interrupt)
    Mutex(LOCK);                 //call syscall mutex to lock mutex (pies is bounded buffer)
    if (pies == 99) {                 //if max bounded buffer
      ascii_fixer(my_str1, GetPid()); //fix number if >=10 and convert to ascii
      cook_too_many[16] = my_str1[0]; //fill in presized array values
      cook_too_many[17] = my_str1[1]; //fill in presized array values
      Write(STDOUT,cook_too_many);    //use syscall to print error message
      Sleep(1);                       //use syscall to sleep and wait 1 second before cooking again
    } else {                          //otherwise, continue making pies
      ascii_fixer(my_str1, GetPid()); //fix number if >=10 and convert to ascii
      ascii_fixer(my_str2, ++pies);   //fix number if >=10 and convert to ascii
      cook_make_pie[7] = my_str1[0];  //fill in presized array values
      cook_make_pie[8] = my_str1[1];  //fill in presized array values
      cook_make_pie[22] = my_str2[0]; //fill in presized array values
      cook_make_pie[23] = my_str2[1]; //fill in presized array values

      Write(STDOUT,cook_make_pie);    //use syscall to print cooking message
      Sleep(1);                       //use syscall to sleep while making the pie
    }
    Mutex(UNLOCK);               //call syscall mutex to unlock mutex for other PIDs queued to use
  }
}

void EaterProc(void) {
  char my_str1[] = "  ";              //empty array for GetPid()
  char my_str2[] = "  ";              //empty array for --pies
  char no_pies[] = "--------Eater  : no pie to eat!"; //error message w/ blank space for number
  char pies_eating[] = "Eater   : eating pie #   ...."; //error message w/ blank space for numbers
  while (1) {                         //forever (until interrupt)
    Mutex(LOCK);                 //call syscall mutex to lock mutex (pies is bounded buffer)
    if (pies == 0) {                  //if below minimum bounded buffer
      ascii_fixer(my_str1, GetPid()); //fix number if >=10 and convert to ascii
      no_pies[14] = my_str1[0];       //fill in presized array values
      no_pies[15] = my_str1[1];       //fill in presized array values
      Write(STDOUT,no_pies);          //use syscall to print error message
      Sleep(1);                       //use syscall to sleep and wait 1 second before eating again
    } else {                          //otherwise continue eating pies
      ascii_fixer(my_str1, GetPid()); //fix number if >=10 and convert to ascii
      ascii_fixer(my_str2, pies--);   //fix number if >=10 and convert to ascii
      pies_eating[6] = my_str1[0];    //fill in presized array values
      pies_eating[7] = my_str1[1];    //fill in presized array values
      pies_eating[22] = my_str2[0];   //fill in presized array values
      pies_eating[23] = my_str2[1];   //fill in presized array values
      Write(STDOUT,pies_eating);      //use syscall to print eating message
      Sleep(1);                       //use syscall to sleep while eating the pie
    }
    Mutex(UNLOCK);               //use syscall mutex to unlock mutex for other PIDs queued to use
  }
}
