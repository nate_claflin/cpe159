//proc.c, 159
//all processes are coded here
//processes do not use kernel space (data.h) or code (handlers, tools)
//all must be done through system service calls
#include "data.h"
#include "syscalls.h" //syscalls library
#include "tools.h"
#include "spede.h"

void CallWaitPidNow(void) {             //shellproc's sigchild handler
  int my_pid, term, child_pid, exit_num;
  char *my_msg;
  child_pid = WaitPid(&exit_num);
  my_msg = "Child    exited, exit #  .\n\r";
  my_msg[6] = child_pid + '0';

  my_msg[23] = exit_num/100 + '0';
  my_msg[24] = (exit_num%100)/10 + '0';
  my_msg[25] = exit_num%10 + '0';
  my_pid = GetPid();
  term = (my_pid%2 == 1) ? TERM1 : TERM2;
  PutStr(term, my_msg);
  
}
void Wrapper(func_p_t sig_handler) {    //takes in pointer to signal handler
  asm("pusha");                         //push all registers so handler does not manipulate them
  sig_handler();                        //call handler
  asm("popa");                          //return all registers, and go back to previous instruction state
}
  
void Ouch(void) {                       //function for ctrl-c event
  int term = (GetPid()%2)? TERM1: TERM2;  //get the terminal
  PutStr(term, "Ow my feelings\n\r");     //do the action then return
}

char* ascii_fixer(char *my_str, int temp) { //takes in string pointer and temp to fix >9 pid values
   if(temp >= 10){                          //if value if > 9
     my_str[0] = temp/10 + '0';             //set each index to the strings modded by 10  
     my_str[1] = temp%10 + '0';             //^^^^^^
  } else {
    my_str[0] = temp + '0';                 //otherwise only set first index
    my_str[1] = ' ';                        //next index is just blank space
  }
  return my_str;                            //give back the pointer
}
void SystemProc(void) {                     //idle process block
  while(1) asm("inb $0x80");                //forever (until interrupt)
}

void ShellProc(void) {                      //process for a shell
  int term, my_pid, forked_pid;             //temp variables
  char my_str[] = "  ";                     //blank string for PID
  char my_msg[] = ": RNEA Shell >";         //team-defined string for shell prompt
  char get_str[100];                        //empty array for input of the string
  
  Signal(SIGINT,Ouch);                      //call signal to inster Ouch into PCB before loop

  while(1) {                                //forever (until interrupt)
  	my_pid = GetPid();                      //get pid
    ascii_fixer(my_str, GetPid());          //clean pid
  	term = ((my_pid)%2 == 1)? TERM1: TERM2; //set terminal based on pid
    PutStr(term, my_str);                   //print out shell prompt pt. 1
    PutStr(term, my_msg);                   //print out shell prompt pt. 2

    GetStr(term, get_str, 100);             //syscall will add null
    if (MyStrcmp(get_str, "fork") == 1) {  //if fork is entered into terminal
    	forked_pid = Fork();                  //call fork command with int as return
    	if (forked_pid == -1) PutStr(term, "ShellProc: Cannot Fork!\n\r"); //if return is -1 then exit

      if (forked_pid > 0) CallWaitPidNow();   //parent waits while child runs
    }
    else if (((MyStrcmp(get_str, "fork&") | (MyStrcmp(get_str, "fork &"))) == 1)) {
        Signal(SIGCHLD, CallWaitPidNow);      //register handler before forking
        forked_pid = Fork();                  //same as before
        if (forked_pid == -1) {
          Signal(SIGCHLD, (func_p_t)0);       //cancel handler
        }
    }
    else if (MyStrcmp(get_str, "exit") == 1) {
      Exit(my_pid * 100);
    }
  }
}
