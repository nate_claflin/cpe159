//handlers.h, 159

#ifndef _HANDLERS_H_
#define _HANDLERS_H_

#include "data.h"
#include "types.h"  //need def of 'func_p_t' below

void NewProcHandler(func_p_t p);  //prototype of NewProcHandler
void TimerHandler(void);          //prototype for TimerHandler
void GetPidHandler(void);
void SleepHandler(void);
void WriteHandler(void);
void MutexLockHandler(void);
void MutexUnlockHandler(void);
void GetCharHandler(int fileno);
void TermHandler(int port);
void PutCharHandler(int fileno);
void ForkHandler(proc_frame_t *proc_frame_p);
void SignalHandler(proc_frame_t *proc_frame_p);
void ExitHandler(proc_frame_t *proc_frame_p);
void WaitPidHandler(proc_frame_t *proc_frame_p);
void InsertWrapper(int pid, func_p_t handler);

#endif
