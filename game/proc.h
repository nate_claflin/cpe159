//proc.h, 159

#ifndef _PROC_H_
#define _PROC_H_
void SystemProc(void);  //prototypes
void UserProc(void);
void CookerProc(void);
void EaterProc(void);
void Player(void);
void Enemy(void);
void Ball(void);
#endif
