//handlers.c, 159

#include "spede.h"
#include "types.h"
#include "data.h"
#include "tools.h"
#include "proc.h"
#include "handlers.h"

void ForkHandler(proc_frame_t *proc_frame_p) {
  int child_pid, delta, *bp;      //working variables
  proc_frame_t *child_frame_p;    //new proc frame for child

  if (ready_q.size == 0) {        //if there is no more available processes
    cons_printf("Kernel Panic: Cannot create more processes!\n"); //print error
    pcb[run_pid].proc_frame_p->EBX = -1;      //return -1 to fork call
    return;                       //return
  }

  child_pid = DeQ(&ready_q);      //otherwise, get child id from ready queue                 
  EnQ(child_pid, &run_q);         //put into run queue                 
  MyBzero((char *)&pcb[child_pid], sizeof(pcb_t));  //clear out the pcb     
  pcb[child_pid].state = RUN;     //set state to run
  MyMemcpy(proc_stack[child_pid], proc_stack[run_pid], PROC_STACK_SIZE);  //copy entire runtime stack contents
  delta = proc_stack[child_pid] - proc_stack[run_pid];  //find distance between parent and child 
  child_frame_p = (proc_frame_t *)((int)pcb[run_pid].proc_frame_p + delta);   //set the location of child frame to parent plus delta
  pcb[child_pid].proc_frame_p = child_frame_p;    //set the child frame into pcb
  pcb[child_pid].ppid = run_pid;                  //set the child's parent ID into pcb
  pcb[child_pid].proc_frame_p->ESP = pcb[child_pid].proc_frame_p->ESP + delta;  //set stack pointer + distance
  pcb[child_pid].proc_frame_p->EBP = pcb[child_pid].proc_frame_p->EBP + delta;  //etc
  pcb[child_pid].proc_frame_p->ESI = pcb[child_pid].proc_frame_p->ESI + delta;  //etc
  pcb[child_pid].proc_frame_p->EDI = pcb[child_pid].proc_frame_p->EDI + delta;  //etc

  bp = (int *)pcb[child_pid].proc_frame_p->EBP; //set the bp pointer to the contents of child's ebp
  while (*bp) {  //while the data bp points to is not 0        
    *bp += delta; //increment the contents of the pointer
    bp = (int *)*bp;  //make new reference to next level in BP chain                    
  }
  pcb[run_pid].proc_frame_p->EBX = child_pid;   //fill register to return to fork syscall
}

void PutCharHandler(int fileno) {
  int i, term;
  char ch;
  if (fileno == TERM1) {
    i = 0;
    term = TERM1_BASE;
  } else {
    i = 1;
    term = TERM2_BASE;
  }
  ch = pcb[run_pid].proc_frame_p->ECX;
  outportb(term, ch);
  EnQ(run_pid, &(terminal_wait_queue[i]));
  pcb[run_pid].state = WAIT;
  run_pid = -1;
}

void GetCharHandler(int fileno) {
 //check for char buffer
 int i;                                     //variable to set which term to pring
 char c;
 if (fileno == TERM1) i = 0;                //if term1, set i to 0
 else i = 1;                                //otherwise i to 1

 if (terminal_buffer[i].size == 0) {        //if there's nothing in the buffer, then need to wait
    EnQ(run_pid,&(terminal_wait_queue[i]));  //put the requesting PID in terminal wait queue
    pcb[run_pid].state = WAIT;           //block the requesting PID
    run_pid = -1;                           //reset PID and continue
 }
 else {                                     //otherwise there is something in the buffer queue
  c = DeQ(&terminal_buffer[i]); //pop value off into the ECX register
   pcb[run_pid].proc_frame_p->ECX = c;
 }
}
void TermHandler(int port) {
  int i,pid,indicator;                                //temp variables for storage
  char ch;                                  //temp char for returning
  if (port == TERM1_BASE) i = 0;            //if term1 base given, i = 0
  else i = 1;                               //otherwise i = 1
  indicator = inportb(port + IIR);

  if (indicator == IIR_RXRDY) {
    ch = inportb(port + DATA);                //get the character data from serial port
    if (terminal_wait_queue[i].size == 0) {   //if no terminals waiting for letter
      EnQ(ch,&terminal_buffer[i]);            //put letter in buffer
    } else {                                  //otherwise
      pid = DeQ(&terminal_wait_queue[i]);     //pop off the PID waiting from terminal
      EnQ(pid, &run_q);                       //put pid in run queue
      pcb[pid].state = RUN;                   //set state from WAIT to RUN 
      pcb[pid].proc_frame_p->ECX = ch;        //give character to the register
      outportb(port + DATA, ch);                //write that character to screen
    }
  } 

  else {
    if (terminal_wait_queue[i].size > 0) {   //if terminal is waiting for letter
      pid = DeQ(&terminal_wait_queue[i]);     //pop off the PID waiting from terminal
      EnQ(pid, &run_q);                       //put pid in run queue
      pcb[pid].state = RUN;                   //set state from WAIT to RUN 
    }
  }

  if (i) outportb(0x20,0x64); 
  else outportb(0x20,0x63);
}

void GetPidHandler(void) {
  pcb[run_pid].proc_frame_p->EAX = run_pid; //fill register value of process frame for run_pid
}

void WriteHandler(void) {
  int i,j,fileno;
  char *p;
  j = 0;

  fileno = pcb[run_pid].proc_frame_p->EBX;        	//get fileno from syscall
  p = (char *)pcb[run_pid].proc_frame_p->ECX;       //get the message address from syscall
  if (fileno == STDOUT) {                         	//if buffer is STDOUT
    cons_printf("%s\n", p);                       	//cons_printf the string
  } else if (fileno == TERM1) {                   	//if fileno is terminal 1
    while (p[j] != '\0') {                          //while there's still string
      outportb(TERM1_BASE, p[j]);                  	//send char to term1 base address, increment pointer
      j++;
      for (i = 0; i < 3000; i++) asm("inb $0x80"); 	//delay for 1 sec
  	}
  } else if (fileno == TERM2) {                   	//if fileno is terminal 2
    while (p[j] != '\0') {                          //while there's still string
      outportb(TERM2_BASE, p[j]);                   //send char to term1 base address, increment pointer
      j++;
      for (i = 0; i < 3000; i++) asm("inb $0x80");  //delay for 1 sec
    }
    
  }
}


void SleepHandler(void) {
  int time;
  time = timer_tick + (pcb[run_pid].proc_frame_p->EBX * 100); //get the time from register 
  pcb[run_pid].wake_time = time;      			  //and add to current system time
  pcb[run_pid].state = SLEEPING;                  //set state
  run_pid = -1;                                    //reset run_pid
}

void NewProcHandler(func_p_t p) { //arg: where proc code starts
  int pid;                                        //initialize pid selector
  if (ready_q.size == 0) {                        //if size of ready_q is 0, panic
    cons_printf("Kernel Panic: cannot create more processes\n");
    return;
  }
  pid = DeQ(&ready_q);                            //get a 'pid' from ready_q
  MyBzero((char *)&pcb[pid], sizeof(pcb_t));      //use tool function MyBzero to clear PCB and runtime stack
  MyBzero((char *)&proc_stack[pid], PROC_STACK_SIZE);   //clear out proc_stack data as well
  if (pid != 0) EnQ(pid, &run_q);                 //use EnQ to add the pid into run_q
  pcb[pid].proc_frame_p = (proc_frame_t *)&proc_stack[pid][PROC_STACK_SIZE - sizeof(proc_frame_t)];   //point proc_frame_p into stack
  pcb[pid].proc_frame_p->EFL = (unsigned int)(EF_DEFAULT_VALUE|EF_INTR);	//bit 9 is EF_INTR and bit 1 is EF_DEFAULT_VALUE
  pcb[pid].proc_frame_p->EIP = (unsigned int)p;// fill out EIP to p (can only be read through stack after call instruction)
  pcb[pid].proc_frame_p->CS = (unsigned int)get_cs();  //fill CS with the return from get_cs() call
  pcb[pid].state = RUN;
}

void MutexLockHandler(void) {
  if (mutex.flag) {                               //if the mutex is free (1) 
    mutex.flag = 0;                               //set mutex as locked
  } 
  else {                                          //otherwise, mutex already locked
    EnQ(run_pid,&(mutex.wait_q));                 //put the requesting PID in wait queue
    pcb[run_pid].state = WAIT;                //set sleeping state, maybe should be WAIT so timerhandler doesnt try to recover
    run_pid = -1;                                 //reset so scheduler can schedule another process
  }
}

void MutexUnlockHandler(void) {
  int pid;
  if (mutex.wait_q.size == 0) {                   //if the wait queue has nothing in it 
    mutex.flag = 1;                               //reset the mutex flag, unlock it. 
  }
  else {                                          //if there are waiting processes
    pid = DeQ(&(mutex.wait_q));                   //get the waiting process
    pcb[pid].state = RUN;                         //set the state to running 
    EnQ(pid,&run_q);                              //enq into run queue for kernel to handle
  }
}

void TimerHandler(void) {                         //count run_time of running process and preempt it if reaching time slice
  int j;
  timer_tick++;
  for (j = 1; j < 20; j++) {                      //loop through pcb to find sleeping tasks
    if ((pcb[j].state == SLEEPING) && (timer_tick >= pcb[j].wake_time)) {  //if state is sleeping and time is up
        EnQ(j, &run_q);                           //queue back to run queue
        pcb[j].state = RUN;                     //alter state from SLEEPING (need to be RUN?)
    }
  }
  outportb(0x20,0x60);                            //dismiss timer event IRQ0;
  
  if (run_pid == 0) {                             //if running process is SystemProc, return
    return;
  }
  
  pcb[run_pid].run_time++;                        //upcount cpu time of running process (think this syntax works)
  
  if (pcb[run_pid].run_time == TIME_SLICE) {      //if it reaches time slice, upgrade/downgrade its state
    EnQ(run_pid, &run_q);                         //queue back to run_q
    run_pid = -1;                                //reset the running pic to -1 (no processes running)
  }
}
