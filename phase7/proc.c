//proc.c, 159
//all processes are coded here
//processes do not use kernel space (data.h) or code (handlers, tools)
//all must be done through system service calls
#include "data.h"
#include "syscalls.h" //syscalls library
#include "tools.h"

char* ascii_fixer(char *my_str, int temp) {
   if(temp >= 10){
     my_str[0] = temp/10 + '0';
     my_str[1] = temp%10 + '0';
  } else {
    my_str[0] = temp + '0';
    my_str[1] = ' ';
  }
  return my_str;
}
void SystemProc(void) {
  while(1) asm("inb $0x80");    //forever (until interrupt)
}

void ShellProc(void) {
  int term, my_pid, forked_pid;
  char my_str[] = "  ";
  char my_msg[] = ": RNEA Shell >";
  char get_str[100];

  while(1) {                         //forever (until interrupt)
  	my_pid = GetPid();
    ascii_fixer(my_str, GetPid());
  	term = ((my_pid)%2 == 1)? TERM1: TERM2;
    PutStr(term, my_str);
    PutStr(term, my_msg);

    GetStr(term, get_str, 100); //syscall will add null
    if (MyStrcmp(get_str, "fork", 4) == 1) {
    	forked_pid = Fork();
    	if (forked_pid == -1) PutStr(term, "ShellProc: Cannot Fork!\n\r");
    }
  }
}
