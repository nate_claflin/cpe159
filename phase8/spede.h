//spede.h

#ifndef _SPEDE_H_
#define _SPEDE_H_

#include <spede/flames.h>
#include <spede/sys/cdefs.h>
#include <spede/stdio.h>
#include <spede/assert.h>

#include <spede/machine/io.h>
#include <spede/machine/proc_reg.h>
#include <spede/machine/seg.h>
#include <spede/machine/asmacros.h>

#include <spede/machine/rs232.h>

#endif
