// syscalls.c
// API calls to kernel system services
#include "spede.h"
#include "types.h"
#include "data.h"
#include "tools.h"
#include "proc.h"
#include "handlers.h"
#include "syscalls.h"

void Signal(int signal_num, func_p_t sig_handler) {
  //send signal event, sigint number and ouch address to handler
  asm("movl $48, %%eax;
      movl %0, %%ebx;
      movl %1, %%ecx;
      int $128"
      :
      : "g" (signal_num), "g" ((int)sig_handler)
      : "eax", "ebx", "ecx"
      );
}
int Fork(void) {
  int ret;

  asm("movl $2, %%eax;
      int $128;
      movl %%ebx, %0"
      : "=g" (ret)
      :
      : "eax", "ebx"
      );
  return ret;
}

void PutStr(int fileno, char *p) {
  int i = 0;            //initialize counter
  while (p[i] != '\0') {   //while the string character is not null
    PutChar(fileno, p[i]); //call putchar to place the character
    i++;                   //
  }
}

void GetStr(int fileno, char *p, int size) {
  int i = 0;            //initialize counter
  char ch = ' ';        //set blank character

  while (i < (size-1)) { //while less than size
    ch = GetChar(fileno);   //get the character from terminal
    if ((ch == (char)13) || (ch == (char)11)) { //if input is newline or ret char
      break;              //leave loop
    }
    p[i] = ch;          //place character in string pointer
    i++;                //increase index
  }
  PutChar(fileno, '\n');
  p[i] = '\0';          //add termination to string
}

void PutChar(int fileno, char ch) {
  asm("movl $104, %%eax;
      movl %0, %%ebx;
      movl %1, %%ecx;
      int $128"
      :
      : "g" (fileno), "g" ((int)ch)
      : "eax", "ebx", "ecx"
  );
}
char GetChar(int fileno) {
  int ch;
  asm("movl $103, %%eax;
      movl %1, %%ebx;
      int $128;
      movl %%ecx, %0"
      : "=g" (ch)
      : "g" (fileno)
      : "eax", "ebx", "ecx"
      
      );
   return (char)ch;
}
void Mutex(int service) {
  asm("movl $102, %%eax;
      movl %0, %%ebx;
      int $128"
      :  
      : "g" (service)
      : "eax", "ebx"
    );
}
/* GetPid takes no arguments, and pushes
  eax, moves 100 as opcode, then calls int
  service which will fetch the current pid
  and return into the eax register, then pops
*/

int GetPid(void) {
   int pid;

   asm("movl $100, %%EAX;   
        int $128;           
        movl %%EAX, %0"    
        : "=g" (pid)         
        :                   
        : "eax"
    );
   //int 128 is what calls the interrupt located at 128

   return pid;
}

/*Write pushes eax, then moves 4 for opcode
  then moves first argument into ebx, and second
  into ecx, then calls interrupt service, then pops
  eax back once completed
*/
void Write(int fileno, char *p) {
  asm("movl $4, %%eax;
      movl %0, %%ebx;
      movl %1, %%ecx;
      int $128"
       :          // no outputs, otherwise, use "=g" (...)
       : "g" (fileno), "g" ((int)p)  
       : "eax", "ebx", "ecx"
       );
 
}

/*sleep pushes eax, then moves 101 for opcode
  then moves first argument into ebx, and second
  into ecx, then calls interrupt service, then pops
  eax back once completed
*/
void Sleep(int time) {
  asm("movl $101, %%eax;
      movl %0, %%ebx;
      int $128"
      :
      : "g" ((int)time)
      : "eax", "ebx"
      );
}
