//tools.h, 159

#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "types.h"  //deed definition of 'q_t' below

void EnQ(int, q_t *); //prototypes for functions
int DeQ(q_t *);
void MyBzero(char *, int);
int MyStrcmp(char *p, char *s, int size);
void MyMemcpy(/*stuff*/);

#endif
