// syscalls.h

#ifndef _SYSCALLS_H_
#define _SYSCALLS_H_

void Mutex(int service);
int GetPid(void);         // no input, 1 return
void Write(int fileno, char *p);
void Sleep(int time);
char GetChar(int fileno);
void PutChar(int fileno, char ch);
void PutStr(int fileno, char *p);
void GetStr(int fileno, char *p, int size);
int Fork(void);
void Signal(int signal_num, func_p_t sig_handler);

#endif
