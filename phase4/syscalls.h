// syscalls.h

#ifndef _SYSCALLS_H_
#define _SYSCALLS_H_

void Mutex(int service);
int GetPid(void);         // no input, 1 return
void Write(int fileno, char *p);
void Sleep(int time);

#endif
