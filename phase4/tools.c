//tools.c, 159

#include "spede.h"
#include "types.h"
#include "data.h"

void MyBzero(char *p, int size) {          //clear DRAM data blocks by filling zeroes
  int i;                                   //initialize counter
  for (i = 0; i < size; i++) {  *p++ = 0;  //loop through given size and clear all indexes
  }
}

int DeQ(q_t *p) {                         //dequeue returns 1st element in array and moves all forward. if queue empty, return -1
  int i;                                  //initialize counter
  int element = -1;                       //initialize return var
  if (p->size == 0) return element;       //if system proc, return -1
  element = p->q[0];                      //set the return element to the PID at head of queue
  p->size--;                              //decrement queue
  for (i = 0; i < p->size; i++) p->q[i] = p->q[i+1];  //loop through queue of new decremented size and shift left
  return element;                        //return either -1 or the PID to remove
}

void EnQ(int element, q_t *p) {
  if (p->size == Q_SIZE) {                //if size field of q_t struct is max
    cons_printf("Kernel Panic: queue is full, cannot EnQ!\n"); //print error
    return;                               //break out
  }
  p->q[p->size] = element;                //put element into the index determined by size
  p->size++;                              //increment size,
}

  
