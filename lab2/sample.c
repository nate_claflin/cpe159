#include <spede/stdio.h>
#include <spede/flames.h>

void DisplayMessage(int i) {
   printf("%d Hello world %d\nECS",i,2*i);
   cons_printf("-->Hello world <--\nCPE/CSC");
}

int main(void) {
  long i;
  int j = 0;
  i = 111;

  for(j = 0; j < 5; j++) {
    DisplayMessage(i);
    i++;
  }
  return 0;
}
