# README #

This is a repository for Nate and Ryan's CPE159 Operating System Pragmatics course from CSU Sacramento

### What is this repository for? ###

This repository is for maintaing the code base of the various phases of the same project throughout the semester

### How do I get set up? ###

No setup required, only a linux system and git

### Contribution guidelines ###

Authored by Nathaniel Claflin

### Breakdown of Phases ###

* The first phase in assignment got context switching functional as a scheduler
* The second phase added onto it with the addition of protected system calls
* The third phase incorporates a typical bounded buffer race condition scenario alleviated by a mutex
* The fourth phase adds functionality for writing data to a terminal
* The fifth phase adds functionality for reading characters from terminal
* The sixth phase allows for reading full strings and serializes the writing of strings to avoid overflowing CPU time
* The seventh phase incorporates the FORK command in its entirety
* The eighth phase incorporates signal handling and process redirection
* The ninth phase incorporates the EXIT command in its entirety
* The (A)th phase utilizes memory pages for stack and program memory
* The (B)th phase creates a large amount of memory, and uses virtual paging to access stack and data